#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <regex.h>
#include <sys/types.h>

#define SYSTEMCTL_PATH "/bin/systemctl"

int check_cmd(char* cmd);
int check_param(char* param);
/*int run(const char * cmd, const char * param) {
	char * const args[4] = {SYSTEMCTL_PATH, cmd, param, NULL};
	execv(SYSTEMCTL_PATH, args);
}*/

void usage() {
  char* line1 = "usage: web-services [start|stop|status|is-active] [service]";
  char* line2 = "\tservice must be in the allowed list";
  fprintf(stderr, "%s\n%s\n", line1, line2);
  exit(-1);
}

int main(int argc, char ** argv) {
    // printf("euid: %d\n", geteuid());
    if ( argc != 3) {
      usage();
    } else if ( check_cmd(argv[1]) && check_param(argv[2]) ) {
			char * const args[4] = {SYSTEMCTL_PATH, argv[1], argv[2], NULL};
			execv(SYSTEMCTL_PATH, args);
    } else {
      usage();
    }
    return 0;
}

int check_cmd (char * cmd) {
  if ( 0 == strncmp(cmd, "status", 7) )
    return 1;
  if ( 0 == strncmp(cmd, "start", 6) )
    return 1;
  if ( 0 == strncmp(cmd, "stop", 5) )
    return 1;
  if ( 0 == strncmp(cmd, "is-active", 10) )
    return 1;
  return 0;
}

int check_param(char * param) {
#define COUNT 4
	const char * patts[COUNT] = {"mcs@([_a-zA-Z0-9])+",
                               "arkserver",
                               "ark-server",
                               "nginx"};
	regex_t regex;
	int reti;
	for (int i = 0; i < COUNT; ++i) {
		reti = regcomp(&regex, patts[i], REG_EXTENDED);
		if ( reti ) {
			fprintf(stderr, "service name pattern isn't a posix regex\n");
			exit(-1);
		}
		reti = regexec(&regex, param, 0, NULL, 0);
		if (!reti)
			return 1;
		regfree(&regex);
	}
  return 0;
}
